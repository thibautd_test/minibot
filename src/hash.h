#pragma once

typedef struct hash {
    struct hash_node* nodes;
    size_t capacity;
    size_t size;
} hash_t;

typedef struct hash_node {
    char *key;
    void *value;
} hash_node_t ;

hash_t* hash_create(size_t capacity);
void hash_destroy(hash_t *hash);
