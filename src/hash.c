#include <stddef.h>
#include <stdlib.h>

#include "hash.h"

hash_t *hash_create(size_t capacity) {
    hash_t *hash = malloc(sizeof(hash_t));
    if (hash == NULL)
        return NULL;
    hash->size = 0;
    hash->capacity = capacity;
    hash->nodes = calloc(capacity, sizeof(hash_node_t));
    if (hash->nodes == NULL) {
        free(hash);
        return NULL;
    }
    return hash;
}

void hash_destroy(hash_t *hash) {
    free(hash->nodes);
    free(hash);
}
