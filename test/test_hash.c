#include "cmocka.h"
#include "hash.h"

#define DEFAULT_HASH_SIZE 2

static int setup(void **state) {
    *state = hash_create(DEFAULT_HASH_SIZE);
    return 0;
}

static int cleanup(void **state) {
    hash_destroy(*state);
    return 0;
}

static void test_hash_create(void **state) {
    hash_t *hash = *state;
    assert_int_equal(hash->capacity, DEFAULT_HASH_SIZE);
    assert_int_equal(hash->size, 0);
    assert_non_null(hash->nodes);
}

int main(void) {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(test_hash_create),
    };
    return cmocka_run_group_tests(tests, setup, cleanup);
}
